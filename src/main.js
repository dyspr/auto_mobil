var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var initSize = 0.1
var dimension = 7
var frame = Math.PI * 0.5
var rotattion = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < dimension; i++) {
    for (var j = 0; j < dimension; j++) {
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(dimension * 0.5)) * boardSize * initSize * sqrt(3) * 0.5, windowHeight * 0.5 + (j - Math.floor(dimension * 0.5)) * boardSize * initSize + (i % 2) * 0.5 * boardSize * initSize - boardSize * initSize * 0.5)
      fill(((i + j) % 2) * 255 + pow(-1, i + j) * (255 - 128) + 128 * (sin(frame + j * (1 / dimension))))
      noStroke()
      ellipse(0, 0, boardSize * initSize * 0.5 + boardSize * initSize * sin(frame) * 0.5, boardSize * initSize * 0.5 + sin(frame) * boardSize * initSize * 0.5)
      pop()

      fill((255 - 128) + 128 * (cos(frame + j * (1 / dimension) * 2)))
      noStroke()
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(dimension * 0.5)) * boardSize * initSize * sqrt(3) * 0.5, windowHeight * 0.5 + (j - Math.floor(dimension * 0.5)) * boardSize * initSize + (i % 2) * 0.5 * boardSize * initSize - boardSize * initSize * 0.5)
      rotate(Math.PI * 0.5 * rotattion)
      arc(0, 0, boardSize * initSize * cos(frame) * 1, boardSize * initSize * cos(frame) * 1, 0, Math.PI * 0.5)
      arc(0, 0, boardSize * initSize * cos(frame) * 1, boardSize * initSize * cos(frame) * 1, Math.PI * 1, Math.PI * 1.5)
      pop()
    }
  }

  frame += deltaTime * 0.001
  if (frame >= Math.PI * 2 + Math.PI * 0.5) {
    frame = Math.PI * 0.5
    rotattion++
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
